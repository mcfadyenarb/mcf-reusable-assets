package com.mcfadyen.assets.lv.pdf;

import java.io.IOException;
import java.util.Map;

import javax.naming.ConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

public class ProcCreatePDF extends GenericService implements PipelineProcessor {

	/** Success Code **/
	private static final int OK = 1;

	/** Failure Code **/
	private static final int KO = 2;

	private static final String STORE_CA = "storeCA";

	private static final String FRA = "fra";

	private PDFGenerationTools pdfGenerationTools;

    private ServiceMap clientReceiptPdfGenerator;

	private VuittonOrderManager orderManager;

    private Map<String, String> clientReceiptFilePath;
	
    private VuittonSiteIdFinderService siteIdFinderService;


	@Override
	public int[] getRetCodes() {
		int[] ret = { KO, OK };
		return ret;
	}


    @Override
    public int runProcess(Object obj, PipelineResult arg1) {
        Map<String, Object> map = (Map<String, Object>) obj;
        VuittonShippingGroupImpl sg = (VuittonShippingGroupImpl) map.get(PipelineConstants.SHIPPINGGROUP);
        VuittonOrder order;
        try {
            order = orderManager.getOrderFromShippingGroup(sg.getId());
            String orderId = order.getId();
            String countryCode = (String) sg.getRepositoryItem().getPropertyValue(RepositoryConstants.COUNTRY_ITEM_NAME);
            StringBuffer xml = getPdfGenerationTools().getXMLForOrder(orderId, sg);
            if (isLoggingDebug()) {
                logDebug("In ProcCreatePDF : ClientReceipt xml : " + xml.toString());
            }
            String fileName = getPdfGenerationTools().generateFileName(sg.getId(), GenericConstants.CR);
            if (isLoggingDebug()) {
                logDebug("In ProcCreatePDF : fileName : " + fileName);
            }
            clientReceiptPDFs(order, countryCode, xml, fileName);
            if (sg instanceof ClickAndCollectShippingGroup) {
                vlogDebug("In ProcCreatePDF : generating collect from store additional pdf for Order {0} and SG {1}", order.getId(), sg.getId());
                String clientReceiptFileName = getPdfGenerationTools().generateFileName(sg.getId(), GenericConstants.CR_CC);
                vlogDebug("In ProcCreatePDF : clientReceiptFileName : {0}", clientReceiptFileName);

                clientReceiptPDFs(order, countryCode, xml, clientReceiptFileName);
            }
        } catch (CommerceException e) {
            logError("CommerceException in ProcCreatePDF : " + e);
        } catch (RepositoryException e) {
            logError("RepositoryException in ProcCreatePDF : " + e);
        } catch (FOPException e) {
            logError("FOPException in ProcCreatePDF : " + e);
        } catch (IOException e) {
            logError("IOException in ProcCreatePDF : " + e);
        } catch (TransformerException e) {
            logError("TransformerException in ProcCreatePDF : " + e);
        }
        return OK;
    }


    private void clientReceiptPDFs(VuittonOrder order, String countryCode, StringBuffer xml, String fileName) throws IOException, FOPException,
            TransformerException {
        if (isCanadianFROrder(order)) {
            vlogDebug("Canadian Order in French . Generating Client Receipt in French");
            PDFDownloader pdfComponent = (PDFDownloader) getClientReceiptPdfGenerator().get(GenericConstants.CA_FR);
            pdfComponent.convertToPDF(xml.toString(), countryCode, fileName, getClientReceiptFilePath().get(countryCode));
        } else if (isChineseOrder(order)) {
            vlogDebug("Chinese Order . Generating Client Receipt in Chinese");
            try {
                PDFDownloader pdfComponent = (PDFDownloader) getClientReceiptPdfGenerator().get(countryCode);
                pdfComponent.convertToPDFwithConfig(xml.toString(), countryCode, fileName, getClientReceiptFilePath().get(countryCode));
            } catch (ConfigurationException | TransformerException | SAXException e) {
                logError("Exception in ProcCreatePDF : " + e);
            }
        } else {
            vlogDebug("Generating Client Receipt in English");
            PDFDownloader pdfComponent = (PDFDownloader) getClientReceiptPdfGenerator().get(countryCode);
            pdfComponent.convertToPDF(xml.toString(), countryCode, fileName, getClientReceiptFilePath().get(countryCode));
        }
    }

	
	private boolean isCanadianFROrder(VuittonOrder order) {
		boolean isCanadianFROrder = false;
		RepositoryItem orderContext = (RepositoryItem) order.getContext();
		if (orderContext == null) {
			if (isLoggingError()) {
				logError("In isCanadianFROrder ::: [" + order.getId() + "] " + "No order context found");
			}
			return isCanadianFROrder;
		}
		String storeReference = orderContext.getPropertyValue(RepositoryConstants.CONTEXT_STORE_REFERENCE_PROPERTY_NAME).toString();
		String language = orderContext.getPropertyValue(RepositoryConstants.CONTEXT_LANGAGE_PROPERTY_NAME).toString();
		if (isLoggingDebug()) {
			logDebug("order : " + order.getId() + " storeReference : " + storeReference + " language : " + language);
		}
		if (STORE_CA.equalsIgnoreCase(storeReference) && FRA.equalsIgnoreCase(language)) {
			isCanadianFROrder = true;
		}
		return isCanadianFROrder;
	}

    private boolean isChineseOrder(VuittonOrder order) {
        return getSiteIdFinderService().isChinaSite(order.getSiteId());
    }

    public VuittonSiteIdFinderService getSiteIdFinderService() {
        return siteIdFinderService;
    }

    public void setSiteIdFinderService(VuittonSiteIdFinderService siteIdFinderService) {
        this.siteIdFinderService = siteIdFinderService;
    }

    public PDFGenerationTools getPdfGenerationTools() {
        return pdfGenerationTools;
    }

    public void setPdfGenerationTools(PDFGenerationTools pdfGenerationTools) {
        this.pdfGenerationTools = pdfGenerationTools;
    }

    public VuittonOrderManager getOrderManager() {
        return orderManager;
    }

    public void setOrderManager(VuittonOrderManager orderManager) {
        this.orderManager = orderManager;
    }

    public Map<String, String> getClientReceiptFilePath() {
        return clientReceiptFilePath;
    }

    public void setClientReceiptFilePath(Map<String, String> clientReceiptFilePath) {
        this.clientReceiptFilePath = clientReceiptFilePath;
    }

    public ServiceMap getClientReceiptPdfGenerator() {
        return clientReceiptPdfGenerator;
    }

    public void setClientReceiptPdfGenerator(ServiceMap clientReceiptPdfGenerator) {
        this.clientReceiptPdfGenerator = clientReceiptPdfGenerator;
    }

}
