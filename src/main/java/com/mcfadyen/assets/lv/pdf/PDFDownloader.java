package com.mcfadyen.assets.lv.pdf;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.naming.ConfigurationException;
import javax.security.auth.login.Configuration;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.xml.sax.SAXException;
// import org.apache.avalon.framework.configuration.Configuration;
// import org.apache.avalon.framework.configuration.ConfigurationException;
// import org.apache.avalon.framework.configuration.DefaultConfigurationBuilder;
// import org.apache.fop.apps.FOPException;
// import org.apache.fop.apps.FOUserAgent;
// import org.apache.fop.apps.Fop;
// import org.apache.fop.apps.FopFactory;
// import org.apache.fop.apps.FopFactoryBuilder;
// import org.apache.fop.apps.MimeConstants;
// import org.xml.sax.SAXException;

// import atg.nucleus.ConfigPathFile;
// import atg.nucleus.ConfigPathFileException;
// import atg.nucleus.GenericService;
// import atg.nucleus.ServiceException;

public class PDFDownloader extends GenericService {

    private Map<String, String> countryXslMapping;

    private String fopConfigPath;

    private Map<String, ConfigPathFile> xslConfigMap;

    private ConfigPathFile fopConfigFile;

    private File fontBaseDir;

    /**
     * Init FOP Configuration for the component
     */
    public void doStartService() throws ServiceException {
        super.doStartService();

        xslConfigMap = new HashMap<String, ConfigPathFile>();

        Map<String, String> map = getCountryXslMapping();
        for (Entry<String, String> entry : map.entrySet()) {

            xslConfigMap.put(entry.getKey(), new ConfigPathFile(entry.getValue()));
        }

    }

    /**
     * create a PDF Stream with FOP from XML data by applying an xsl transformation specific to each country
     *
     * @param xmlData xml data matching the structure expected by the xsl
     * @param countryCode a code that must exists in countryXslMap property
     */

    public void convertToPDF(String xmlData, String countryCode, String fileName, String filePath) throws IOException, FOPException,
            TransformerException {

        StreamSource xmlSource = new StreamSource(new StringReader(xmlData));

        // the XSL FO
        ConfigPathFile xsl = xslConfigMap.get(countryCode);

        // File config = new File("E:\\fop\\GM\\JP\\fonts");

        // File configFile = new File("E:\\fop\\GM\\JP\\config.xml");

        DefaultConfigurationBuilder cfgBuilder = new DefaultConfigurationBuilder();
        Configuration cfg = null;
        try {
            // cfg = cfgBuilder.build(new FileInputStream(getFopConfigFile()));
            cfg = cfgBuilder.build(getFopConfigFile().getAsStream());
        } catch (Exception e) {
            logError("Exception in convertToPDF of PDFDownloader : " + e);
            e.printStackTrace();
        }
        // create an instance of fop factory
        FopFactoryBuilder configBuilder = new FopFactoryBuilder(getFontBaseDir().toURI());
        FopFactoryBuilder fopFactoryBuilder = configBuilder.setConfiguration(cfg);
        FopFactory fopFactory = fopFactoryBuilder.build();
        // a user agent is needed for transformation
        FOUserAgent foUserAgent = fopFactory.newFOUserAgent();

        // Setup output
        File file = new File(filePath, fileName.concat(".TMP"));
        OutputStream out = new FileOutputStream(file);

        try {
            // Construct fop with desired output format
            Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);

            // Setup XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource(xsl.getAsStream()));

            // Resulting SAX events (the generated FO) must be piped through to
            // FOP
            Result res = new SAXResult(fop.getDefaultHandler());

            // Start XSLT transformation and FOP processing
            // That's where the XML is first transformed to XSL-FO and then
            // PDF is created
            transformer.transform(xmlSource, res);

        } catch (ConfigPathFileException e) {
            logError("ConfigPathFileException in convertToPDF : " + e);
            e.printStackTrace();
        } finally {
            out.close();

        }

        boolean result = file.renameTo(new File(filePath, fileName));
        if (!result) {
            vlogError("Error renaming file from {0} to {1}", fileName.concat(".TMP"), fileName);
        }
    }

    public void convertToPDFwithConfig(String xmlData, String countryCode, String fileName, String filePath) throws IOException,
            TransformerException, ConfigurationException, SAXException {

        StreamSource xmlSource = new StreamSource(new StringReader(xmlData));
        ConfigPathFile xsl = xslConfigMap.get(countryCode);
        Configuration cfg = null;

        // Setup output
        File file = new File(filePath, fileName.concat(".TMP"));
        OutputStream out = new FileOutputStream(file);

        DefaultConfigurationBuilder cfgBuilder = new DefaultConfigurationBuilder();
        try {
            // cfg = cfgBuilder.build(PDFDownloader.class.getClassLoader().getResourceAsStream(getFopConfigPath()));
            cfg = cfgBuilder.build(new FileInputStream(getFopConfigPath()));
        } catch (Exception e) {
            logError("Exception in convertToPDF of PDFDownloader : " + e);
        }

        // create an instance of fop factory
        FopFactoryBuilder configBuilder = new FopFactoryBuilder(getFontBaseDir().toURI());
        FopFactoryBuilder fopFactoryBuilder = configBuilder.setConfiguration(cfg);
        FopFactory fopFactory = fopFactoryBuilder.build();
        // FopFactory fopFactory = FopFactory.newInstance();
        // a user agent is needed for transformation
        FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
        // fopFactory.setUserConfig(cfg);
        // Construct fop with desired output format
        Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);

        // Setup XSLT
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = factory.newTransformer(new StreamSource(xsl.getAsStream()));

            // Resulting SAX events (the generated FO) must be piped through to FOP

            Result res = new SAXResult(fop.getDefaultHandler());

            // Start XSLT transformation and FOP processing

            transformer.transform(xmlSource, res);

        } catch (ConfigPathFileException e) {
            logError("Exception in convertToPDFwithConfig : " + e);
        }

        boolean result = file.renameTo(new File(filePath, fileName));
        if (!result) {
            vlogError("Error renaming file from {0} to {1}", fileName.concat(".TMP"), fileName);
        }

    }

    public ByteArrayOutputStream makePDF(String xmlData, String countryCode) throws IOException, FOPException, TransformerException {
        if (xmlData == null || xmlData.length() == 0) {
            logError("Can't generate PDF with null or empty data");
            return null;
        }
        if (countryCode == null || countryCode.length() == 0) {
            logError("Can't generate PDF with null or empty countryCode");
            return null;
        }

        StreamSource xmlSource = new StreamSource(new StringReader(xmlData));

        // the XSL FO
        ConfigPathFile xsl = xslConfigMap.get(countryCode);

        DefaultConfigurationBuilder cfgBuilder = new DefaultConfigurationBuilder();
        Configuration cfg = null;
        try {
            // cfg = cfgBuilder.build(new FileInputStream(getFopConfigFile()));
            cfg = cfgBuilder.build(getFopConfigFile().getAsStream());
        } catch (Exception e) {
            logError("Exception in makePDF of PDFDownloader : " + e);
            e.printStackTrace();
        }
        // create an instance of fop factory
        FopFactoryBuilder configBuilder = new FopFactoryBuilder(getFontBaseDir().toURI());
        FopFactoryBuilder fopFactoryBuilder = configBuilder.setConfiguration(cfg);
        FopFactory fopFactory = fopFactoryBuilder.build();

        // Specify the font directory
        // fopFactory.setFontBaseURL(getFontBaseDir().getAbsolutePath());
        // a user agent is needed for transformation
        FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
        // Setup output
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            // Construct fop with desired output format
            Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);

            // Setup XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource(xsl.getAsStream()));

            // Resulting SAX events (the generated FO) must be piped through to FOP
            Result res = new SAXResult(fop.getDefaultHandler());

            // Start XSLT transformation and FOP processing
            // That's where the XML is first transformed to XSL-FO and then
            // PDF is created
            transformer.transform(xmlSource, res);
        } catch (ConfigPathFileException e) {
            logError("ConfigPathFileException in makePDF : " + e);
            e.printStackTrace();
        }
        return out;
    }

    public Map<String, String> getCountryXslMapping() {
        return countryXslMapping;
    }

    public void setCountryXslMapping(Map<String, String> countryXslMapping) {
        this.countryXslMapping = countryXslMapping;
    }

    public Map<String, ConfigPathFile> getXslConfigMap() {
        return xslConfigMap;
    }

    public void setXslConfigMap(Map<String, ConfigPathFile> xslConfigMap) {
        this.xslConfigMap = xslConfigMap;
    }

    public String getFopConfigPath() {
        return fopConfigPath;
    }

    public void setFopConfigPath(String fopConfigPath) {
        this.fopConfigPath = fopConfigPath;
    }

    public ConfigPathFile getFopConfigFile() {
        return fopConfigFile;
    }

    public void setFopConfigFile(ConfigPathFile fopConfigFile) {
        this.fopConfigFile = fopConfigFile;
    }

    public File getFontBaseDir() {
        return fontBaseDir;
    }

    public void setFontBaseDir(File fontBaseDir) {
        this.fontBaseDir = fontBaseDir;
    }

}
