/**
 * 
 */
package com.mcfadyen.assets.lv.pdf;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.management.Query;
import javax.print.attribute.standard.Media;
import javax.security.auth.login.Configuration;

// TODO: Auto-generated Javadoc
/**
 * The Class PDFGenerationTools.
 */
public class PDFGenerationTools extends GenericService {

    /** The Constant CDATAS. */
    public static final String CDATAS = "<![CDATA[";

    /** The Constant CDATAE. */
	public static final String CDATAE = "]]>";

    /** The Constant STORE_CA. */
	private static final String STORE_CA = "storeCA";

    /** The Constant FRA. */
	private static final String FRA = "fra";

    /** The Constant ASSETS. */
    private static final String ASSETS = "/assets/";

    /** The Constant SHOP_US. */
    private static final String SHOP_US = "shopUS";

    /** The Constant SHOP_GB. */
    private static final String SHOP_GB = "shopGB";

    /** The Constant SHOP_JP. */
    private static final String SHOP_JP = "shopJP";

    /** The user repository. */
	private Repository userRepository;

    /** The order manager. */
	private OrderManager orderManager;

    /** The product catalog. */
	private Repository productCatalog;

    /** The country CN. */
    private String countryCN;

    /** The file path client receipt. */
	private String filePathClientReceipt;

    /** The personalization manager. */
	private PersonalizationManager personalizationManager;

    /** The perfume manager. */
	private PerfumeManager perfumeManager;

    /** The product manager. */
	private ProductManager productManager;

    /** The sku finder. */
	private SkuFinder skuFinder;

    /** The scene 7 manager. */
	private Scene7Manager scene7Manager;

    /** The configuration. */
    private Configuration configuration;

    /** The time zone helper. */
	private SiteTimeZoneHelper timeZoneHelper;

    /** The efapiao helper. */
    private EfapiaoHelper efapiaoHelper;

    /** The efapiao configuration. */
    private EfapiaoConfiguration efapiaoConfiguration;

    /** The site id finder service. */
    private VuittonSiteIdFinderService siteIdFinderService;

    /** The policy text. */
    private Map<String, String> policyText;

    /** The payment text en. */
    private String paymentTextEn;

    /** The payment text fr. */
    private String paymentTextFr;

    /** The date pattern. */
    private Map<String, String> datePattern;

    /** The language translations. */
    private Map<String, String> languageTranslations;

    /** The lv logo. */
    private String lvLogo;

    /** The site finder. */
    private SiteFinder siteFinder;

    /** The payment service. */
    private CyberSourcePaymentService mPaymentService;

    /** The site context manager. */
    private VuittonSiteContextManager siteContextManager;

    /** The m master card. */
    private String mMasterCard;

    /** The american express. */
    private String americanExpress;

    /**
     * Gets the american express.
     *
     * @return the american express
     */
    public String getAmericanExpress() {
        return americanExpress;
    }

    /**
     * Sets the american express.
     *
     * @param pAmericanExpress the new american express
     */
    public void setAmericanExpress(String pAmericanExpress) {
        americanExpress = pAmericanExpress;
    }

    /** The m american express CC type. */
    private String mAmericanExpressCCType;

    /** The m master card CC type. */
    private String mMasterCardCCType;

    /** The m visa CC type. */
    private String mVisaCCType;

    /** The m visa. */
    private String mVisa;

    /** The m discover CC type. */
    private String mDiscoverCCType;

    /** The m discover. */
    private String mDiscover;

    private List<String> mSitesToDisplayCreditCard;

    private int coordReductionFactor;

    public int getCoordReductionFactor() {
        return coordReductionFactor;
    }

    public void setCoordReductionFactor(int coordReductionFactor) {
        this.coordReductionFactor = coordReductionFactor;
    }

    /**
     * Gets the visa CC type.
     *
     * @return the visa CC type
     */
    public String getVisaCCType() {
        return mVisaCCType;
    }

    /**
     * Sets the visa CC type.
     *
     * @param pVisaCCType the new visa CC type
     */
    public void setVisaCCType(String pVisaCCType) {
        mVisaCCType = pVisaCCType;
    }

    /**
     * Gets the visa.
     *
     * @return the visa
     */
    public String getVisa() {
        return mVisa;
    }

    /**
     * Sets the visa.
     *
     * @param pVisa the new visa
     */
    public void setVisa(String pVisa) {
        mVisa = pVisa;
    }

    /**
     * Gets the discover CC type.
     *
     * @return the discover CC type
     */
    public String getDiscoverCCType() {
        return mDiscoverCCType;
    }

    /**
     * Sets the discover CC type.
     *
     * @param pDiscoverCCType the new discover CC type
     */
    public void setDiscoverCCType(String pDiscoverCCType) {
        mDiscoverCCType = pDiscoverCCType;
    }

    /**
     * Gets the discover.
     *
     * @return the discover
     */
    public String getDiscover() {
        return mDiscover;
    }

    /**
     * Sets the discover.
     *
     * @param pDiscover the new discover
     */
    public void setDiscover(String pDiscover) {
        mDiscover = pDiscover;
    }
    /**
     * Gets the american express CC type.
     *
     * @return the american express CC type
     */
    public String getAmericanExpressCCType() {
        return mAmericanExpressCCType;
    }

    /**
     * Sets the american express CC type.
     *
     * @param pAmericanExpressCCType the new american express CC type
     */
    public void setAmericanExpressCCType(String pAmericanExpressCCType) {
        mAmericanExpressCCType = pAmericanExpressCCType;
    }

    /**
     * Gets the master card CC type.
     *
     * @return the master card CC type
     */
    public String getMasterCardCCType() {
        return mMasterCardCCType;
    }

    /**
     * Sets the master card CC type.
     *
     * @param pMasterCardCCType the new master card CC type
     */
    public void setMasterCardCCType(String pMasterCardCCType) {
        mMasterCardCCType = pMasterCardCCType;
    }


    /**
     * Gets the master card.
     *
     * @return the master card
     */
    public String getMasterCard() {
        return mMasterCard;
    }

    /**
     * Sets the master card.
     *
     * @param pMasterCard the new master card
     */
    public void setMasterCard(String pMasterCard) {
        mMasterCard = pMasterCard;
    }

    /**
     * Gets the site context manager.
     *
     * @return the site context manager
     */
    public VuittonSiteContextManager getSiteContextManager() {
        return siteContextManager;
    }

    /**
     * Sets the site context manager.
     *
     * @param siteContextManager the new site context manager
     */
    public void setSiteContextManager(VuittonSiteContextManager siteContextManager) {
        this.siteContextManager = siteContextManager;
    }

    /** The site config. */
    private SiteConfiguration siteConfig;

    /** The site manager. */
    private SiteContextManager siteManager;

    /**
     * Gets the site manager.
     *
     * @return the site manager
     */
    public SiteContextManager getSiteManager() {
        return siteManager;
    }

    /**
     * Sets the site manager.
     *
     * @param siteManager the new site manager
     */
    public void setSiteManager(SiteContextManager siteManager) {
        this.siteManager = siteManager;
    }

    /**
     * Gets the site config.
     *
     * @return the site config
     */
    public SiteConfiguration getSiteConfig() {
        return siteConfig;
    }

    /**
     * Sets the site config.
     *
     * @param siteConfig the new site config
     */
    public void setSiteConfig(SiteConfiguration siteConfig) {
        this.siteConfig = siteConfig;
    }

    /**
     * Gets the payment service.
     *
     * @return the payment service
     */
    public CyberSourcePaymentService getPaymentService() {
        return mPaymentService;
    }

    /**
     * Sets the payment service.
     *
     * @param mPaymentService the new payment service
     */
    public void setPaymentService(CyberSourcePaymentService mPaymentService) {
        this.mPaymentService = mPaymentService;
    }

    private Repository internalProfileRepository;

    /**
     * Gets the XML for order.
     *
     * @param orderId the order id
     * @param sg the sg
     * @return the XML for order
     * @throws RepositoryException the repository exception
     */
    public StringBuffer getXMLForOrder(String orderId, ShippingGroup sg) throws RepositoryException {

		VuittonOrderImpl order = null;
        String ccType = null;
        String ccTypeToDisp = null;
        String ccNumber = null;
        VuittonPaymentGroupManagerImpl paymManager = (VuittonPaymentGroupManagerImpl) getOrderManager().getPaymentGroupManager();
		try {
			order = (VuittonOrderImpl) getOrderManager().loadOrder(orderId);
            if (null == order) {
                return new StringBuffer();
            }
		} catch (CommerceException ce) {
			logError("CommerceException loading order id = " + orderId, ce);

		}
		// Getting Country code and Site Id to check for country specific xml changes 
        String countryCode = (String) ((VuittonShippingGroupImpl) sg).getRepositoryItem().getPropertyValue(
                RepositoryConstants.COUNTRY_ITEM_NAME);
		String firstName = "";
		String lastName = "";
        boolean PayPalExists = Boolean.FALSE;
        boolean BankTransferExists = Boolean.FALSE;
        String storeReference = null;
        String imagesDomainName = getConfiguration().getImagesDomainName();
        imagesDomainName = imagesDomainName + ASSETS + getLvLogo();
		
        boolean isChineseOrder = isChineseOrder(order);
        boolean isKoreanOrder = isKoreanOrder(order);
        List<PaymentGroup> paymentGroups = null;
        try {
            paymentGroups = paymManager.getPaymentGroupsByShippingGroup(sg);
        } catch (CommerceException commerceEx) {
            if (isLoggingError()) {
                logError("CommerceException while retriving payment information" + commerceEx);
            }
        }
        VuittonCreditCardStatus status = null;
        VuittonPaypal vpp = null;
        VuittonBankTransfer vbt = null;
        VuittonCreditCard creditCardPayment = null;
        VuittonCreditCard pg = null;
        if (null != paymentGroups) {
            for (PaymentGroup paymentGroup : paymentGroups) {
                if (paymentGroup instanceof IVuittonPaypal) {
                    vpp = (VuittonPaypal) paymentGroup;
                    break;
                }
                if (paymentGroup instanceof VuittonCreditCard) {
                    creditCardPayment = (VuittonCreditCard) paymentGroup;
                    break;
                }
                if (paymentGroup instanceof VuittonBankTransfer) {
                    vbt = (VuittonBankTransfer) paymentGroup;
                    break;
                }
            }
        }

        if (creditCardPayment != null) {
            ccType = creditCardPayment.getCreditCardType();
        }
        if (vpp != null) {
            ccTypeToDisp = "PayPal";
            PayPalExists = Boolean.TRUE;
        }
        if (vbt != null) {
            ccTypeToDisp = "Bank Tranfer";
            BankTransferExists = Boolean.TRUE;
        }
        if (!StringUtils.isBlank(ccType)) {
        if (ccType.equalsIgnoreCase(getAmericanExpressCCType())) {
            ccTypeToDisp = getAmericanExpress();
        }
        if (ccType.equalsIgnoreCase(getMasterCardCCType())) {
            ccTypeToDisp = getMasterCard();
        }
        if (ccType.equalsIgnoreCase(getVisaCCType())) {
            ccTypeToDisp = getVisa();
        }
        if (ccType.equalsIgnoreCase(getDiscoverCCType())) {
            ccTypeToDisp = getDiscover();
        }
        String merchantIdType = order.getMerchantIdType();
        if (!RepositoryConstants.DEFAULT_INVOICING_COUNTRY.equalsIgnoreCase(order.getInvoicingCountry())) {
            merchantIdType = merchantIdType + order.getInvoicingCountry();
        }

        String subId = null;
        VuittonCreditCardInfo info = null;
            pg = (VuittonCreditCard) getCreditCard(paymentGroups);
            status = (VuittonCreditCardStatus) getPaymentStatus(pg.getAuthorizationStatus());
            if (null != status) {
                subId = status.getSubscriptionId();
            }
        String profileId = order.getProfileId();
        RepositoryItem orderContext = (RepositoryItem) order.getContext();
        if (orderContext != null) {
            storeReference = orderContext.getPropertyValue(RepositoryConstants.CONTEXT_STORE_REFERENCE_PROPERTY_NAME).toString();
        } else {

                vlogError("In getXMLForOrder : order {0} - No order context found ", order.getId());

        }

            info = getPaymentService().getCreditCardInfo(merchantIdType, subId, profileId);

            try {
                if (storeReference != null) {
                    setupExecutionContext(storeReference);
                }
            } catch (StoreException storeException) {
                if (isLoggingError()) {
                    logError("StoreException while setting up Execution Context:::" + storeException);
                }
            }
        if (info != null && info.getCreditCardNumber() != null) {
            ccNumber = getLastnCharacters(info.getCreditCardNumber(), 4);
        }
        }
        // build the xml String containing the Gift message
		StringBuffer xml = new StringBuffer();
		xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		xml.append("<items>");
		// name of the sender (may be used or not)
		if (order.getProfileId() != null) {
			RepositoryItem user = null;
            Address address = null;

			try {
				user = getUserRepository().getItem(order.getProfileId(), "user");
                VuittonPaymentGroup paymentGrp = (VuittonPaymentGroup) order.getPaymentGroups().get(0);
                address = paymentGrp.getBillingAddress();

			} catch (RepositoryException re) {
				if (isLoggingError()) {
					logError(order + " : Error trying to retreive user repository item from order.");
				}
			}
			xml.append("<sender>");
            xml.append("<logo>");
            xml.append(imagesDomainName);
            xml.append("</logo>");
            xml.append("<OrderOrigin>");
            if (isJPOrder(order)) {
                xml.append(retrieveAgentDetails(order));
            } else {
                xml.append(order.getOriginOfOrder());
            }
			xml.append("</OrderOrigin>");
			xml.append("<OrderID>");
			xml.append(orderId + "/" + sg.getId());
			xml.append("</OrderID>");
            xml.append("<TimeStamp>");
            SiteTimeZoneHelper zoner = getTimeZoneHelper();
            Date currentSiteTime = zoner.getCurrentTimeFromSite(order.getSiteId());
            xml.append(getTimeStamp(currentSiteTime));
            xml.append("</TimeStamp>");
            if (isCanadianFROrder(order)) {
                xml.append("<Payment>");
                xml.append(getPaymentTextFr());
                xml.append("</Payment>");
            } else if (isUSOrder(order) || isCanadianOrder(order) || isGBOrder(order)) {
                xml.append("<Payment>");
                xml.append(getPaymentTextEn());
                xml.append("</Payment>");
            }
            if (isCanadianFROrder(order) || isUSOrder(order) || isCanadianOrder(order) || isGBOrder(order) || displayCreditCardDetails(order,
                    ccNumber)) {
                xml.append("<PaidVia>");
                if (!PayPalExists && !BankTransferExists && StringUtils.isNotBlank(ccType)) {
                    xml.append(ccTypeToDisp + " " + "(" + ccNumber + ")");
                } else {
                    xml.append(ccTypeToDisp);
                }
                xml.append("</PaidVia>");
            }
            xml.append("<Date>");
            xml.append(getDate(currentSiteTime));
            xml.append("</Date>");
            xml.append("<LocaleSpecificDate>");
            xml.append(getLocaleSpecificDate(currentSiteTime, order, countryCode));
            xml.append("</LocaleSpecificDate>");
			if (user.getPropertyValue(RepositoryConstants.FIRST_NAME_PROPERTY_NAME) != null) {
				firstName = (String) user.getPropertyValue(RepositoryConstants.FIRST_NAME_PROPERTY_NAME);
            } else {
                firstName = address.getFirstName();
			}
            String address1 = address.getAddress1();
            String address2 = address.getAddress2();
            String address3 = address.getAddress3();
            String city = address.getCity();
            String state = address.getState();
            String postalcode = address.getPostalCode();
            postalcode = StringUtils.isBlank(postalcode) ? StringUtils.EMPTY : postalcode;
            String county = address.getCounty();
            String country = address.getCountry();
			if (isChineseOrder) {
                country = getCountryCN();
            }

            address1 = replaceSpecialChar(address1);
            address2 = replaceSpecialChar(address2);
            address3 = replaceSpecialChar(address3);

			xml.append("<Address1>");
            if (isKoreanOrder) {
                vlogDebug("Appending address 1 tag to xml for Korea: {0} {1}", address2, address3);
                buildXmlString(xml, address2, address3);
            } else {
                vlogDebug("Appending address 1 tag to xml: {0}", address1);
                xml.append(address1);
            }
			xml.append("</Address1>");
			if (address2 != null) {
				xml.append("<Address2>");
                vlogDebug("Appending address 2 tag to xml: {0}", address2);
                xml.append(address2);
				xml.append("</Address2>");
			}
			xml.append("<Address3>");
            if (isChineseOrder) {
                vlogDebug("Appending address 3 tag to xml for China: {0} {1} {2} {3}", country, state, city, county);
                xml.append(country + " " + state + " " + city + " " + county);
            } else if (isKoreanOrder) {
                vlogDebug("Appending address 3 tag to xml for Korea: {0} {1} {2} {3}", country, state, city, address1);
                buildXmlString(xml, country, state, city, address1);
            } else {
                vlogDebug("Appending address 3 tag to xml: {0} {1} {2} {3}", city, state, postalcode, country);
                if (!StringUtils.isBlank(state)) {
                    xml.append(replaceSpecialChar(city + " " + state + " " + postalcode + " " + country));
                } else {
                    xml.append(replaceSpecialChar(city + " " + postalcode + " " + country));
                }

            }
			xml.append("</Address3>");

			xml.append("<Name>");

			if (user.getPropertyValue(RepositoryConstants.LAST_NAME_PROPERTY_NAME) != null) {
				lastName = (String) user.getPropertyValue(RepositoryConstants.LAST_NAME_PROPERTY_NAME);
            } else {
                lastName = address.getLastName();
			}
			
            if (isChineseOrder || isKoreanOrder) {
                vlogDebug("Appending name tag to xml for country {0}: {1} {2}", country, lastName, firstName);
                xml.append(replaceSpecialChar(lastName + " " + firstName));
			}else{
                vlogDebug("Appending name tag to xml for country {0}: {1} {2}", country, firstName, lastName);
                xml.append(replaceSpecialChar(firstName + " " + lastName));
			}
			
			xml.append("</Name>");

            if (isChineseOrder) {
                try {
                    if (efapiaoHelper.isFapiaoCreated((VuittonShippingGroup) sg)) {
                        vlogDebug("Appending fapiao tag to xml for China");
                        xml.append("<Fapiao>").append(efapiaoConfiguration.getFapiaoIssued()).append("</Fapiao>");
                    }
                } catch (CommerceException e) {
                    vlogError("Error checking Fapiao");
                }
			}
			
			boolean isCanadianFROrder = isCanadianFROrder(order);
			if (isCanadianFROrder) {
			if (isLoggingDebug()) {
				logDebug("In getXMLForOrder : order : " + order.getId() + " adding policy in french" );
			}
			xml.append("<Policy>");
                xml.append(getPolicyText().get(GenericConstants.CA_FR));
			xml.append("</Policy>");
            } else {
				if (isLoggingDebug()) {
                    logDebug("In getXMLForOrder : order : " + order.getId() + " adding policy as per : " + countryCode);
				}
				xml.append("<Policy>");
                xml.append(getPolicyText().get(countryCode));
				xml.append("</Policy>");
			}
			xml.append("</sender>");
			List commerceItemShippingGroupRelationships = sg.getCommerceItemRelationships();
			for (Object relationShip : commerceItemShippingGroupRelationships) {
				xml.append("<item>");
				CommerceItemRelationship commerceItemRelationship = (CommerceItemRelationship) relationShip;
				String skuId = commerceItemRelationship.getCommerceItem().getCatalogRefId();
				Long quantity = commerceItemRelationship.getQuantity();
				xml.append("<quantity>");
				xml.append(quantity);
				xml.append("</quantity>");
				xml.append("<code>");
				xml.append(skuId);
				xml.append("</code>");
				xml.append("<description>");
				RepositoryItem sku = (RepositoryItem) commerceItemRelationship.getCommerceItem().getAuxiliaryData()
						.getCatalogRef();
				String displayName = (String) sku.getPropertyValue(RepositoryConstants.DISPLAY_NAME_PROPERTY_VALUE);
				if (StringUtils.isEmpty(displayName)) {
					displayName = (String) sku.getPropertyValue(RepositoryConstants.DISPLAY_NAME_PROPERTY_VALUE_DEFAULT);
				}
                if (displayName != null) {
                    displayName = displayName.replaceAll("&", "&amp;");
                }
				xml.append(displayName);
				xml.append("</description>");
				xml.append("<associate>");
                xml.append(order.getOriginOfOrder());
				xml.append("</associate>");
				boolean isCanadianOrder = isCanadianOrder(order);
				if (isCanadianOrder) {
				if (isLoggingDebug()) {
					logDebug("In getXMLForOrder : order : " + order.getId() + " canadian order . Adding tps text" );
				}
				xml.append("<tps>");
				xml.append("tps text");
				xml.append("</tps>");
				}
				xml.append("</item>");
			}

			xml.append("</items>");
		}
		return xml;
	}

    private String retrieveAgentDetails(VuittonOrderImpl order) {
        String agentName = null;
        String salesAssociateCode = order.getSalesAssociateCode();
        if (!salesAssociateCode.equalsIgnoreCase(RepositoryConstants.WEB)) {
            RepositoryView agentView;
            try {
                agentView = getInternalProfileRepository().getView(RepositoryConstants.AGENT);

                QueryBuilder qb = agentView.getQueryBuilder();

                QueryExpression saCodeProperty = qb.createPropertyQueryExpression(RepositoryConstants.SALES_ASSOCIATE_CODE);
                QueryExpression saCodeValue = qb.createConstantQueryExpression(salesAssociateCode);
                Query comparisonQuery = qb.createComparisonQuery(saCodeProperty, saCodeValue, QueryBuilder.EQUALS);

                RepositoryItem[] items = agentView.executeQuery(comparisonQuery);
                if (items != null) {
                    String firstName = (String) items[0].getPropertyValue(RepositoryConstants.FIRSTNAME);
                    String lastName = (String) items[0].getPropertyValue(RepositoryConstants.LASTNAME);
                    agentName = firstName.concat(" ").concat(lastName);
                }
            } catch (RepositoryException re) {
                vlogError("RepositoryException in retrieveAgentDetails - ", re);
            }
        }
        if (null == agentName) {
            agentName = order.getOriginOfOrder();
        }
        return agentName;
    }

    /**
     * Gets the payment status.
     *
     * @param authorizationStatus the authorization status
     * @return the payment status
     */
    private PaymentStatus getPaymentStatus(List<PaymentStatus> authorizationStatus) {
        Iterator<PaymentStatus> itPg = authorizationStatus.iterator();
        while (itPg.hasNext()) {
            PaymentStatus payStatus = itPg.next();
            if (payStatus instanceof VuittonCreditCardStatus) {
                return payStatus;
            }
        }

        return null;
    }

    /**
     * Look for a Credit Card on the paymentGroups.
     *
     * @param paymentGroups the payment groups
     * @return the credit card
     */
    @SuppressWarnings("unchecked")
    private PaymentGroup getCreditCard(List<PaymentGroup> paymentGroups) {

        Iterator<PaymentGroup> itPg = paymentGroups.iterator();
        while (itPg.hasNext()) {
            PaymentGroup pg = itPg.next();
            if (pg instanceof VuittonCreditCard) {
                return pg;
            }
        }

        return null;
    }

    /**
     * Sets the up execution context.
     *
     * @param storeReference the new up execution context
     * @throws StoreException the store exception
     */
    private void setupExecutionContext(String storeReference) throws StoreException {
        SiteConfiguration site = getSiteFinder().findSiteConfigurationByStore(storeReference);

        if (site == null) {
            if (isLoggingError()) {
                logError("setupExecutionContext - Site is null for storeRef " + storeReference + ", cannot setup execution context.");
            }
            return;
        }
        Language language = site.getDefaultLanguage();
        if (language == null) {
            if (isLoggingError()) {
                logError("setupExecutionContext - Default language is null for storeRef " + storeReference
                        + ", cannot setup execution context.");
            }
            return;
        }
        getSiteContextManager().setCurrentSiteContext(site, language.getCodeIso2(), null);
    }

    /**
     * Replace special char.
     *
     * @param address the address
     * @return the string
     */
    private String replaceSpecialChar(String address) {

        String result = null;

        if (null != address) {
            result = address.replaceAll("&", "&amp;");
            result = result.replaceAll("'", "&apos;");
            result = result.replaceAll("<", "&lt;");
            result = result.replaceAll(">", "&gt;");
        }
        return result;
    }

    /**
     * Gets the lastn characters.
     *
     * @param inputString the input string
     * @param subStringLength the sub string length
     * @return the lastn characters
     */
    public String getLastnCharacters(String inputString, int subStringLength) {
        int length = inputString.length();
        if (length <= subStringLength) {
            return inputString;
        }
        int startIndex = length - subStringLength;
        return inputString.substring(startIndex);
    }

    /**
     * Builds the xml string.
     *
     * @param xml the xml
     * @param param the param
     */
    private void buildXmlString(StringBuffer xml, String... param) {
        for (String var : param) {
            xml.append(StringUtils.isNotEmpty(var) ? var + " " : "");
        }
    }

    /**
     * Get the locale for given siteId.
     *
     * @param siteId the site id
     * @return the site locale
     */
    public Locale getSiteLocale(String siteId) {
        SiteConfiguration siteConfiguration = getSiteFinder().findSiteConfigurationBySite(siteId);
        Language language = siteConfiguration.getDefaultLanguage();
        if (siteConfiguration == null || language == null) {
            return null;
        }
        Country country = siteConfiguration.getDefaultCountry();
        String countryIso2 = country.getCountryCodeISO2();
        String languageIso2 = language.getCodeIso2();
        return new Locale(languageIso2, countryIso2);
    }

    /**
     * To check for Korean order.
     *
     * @param order the order
     * @return true, if is korean order
     */
    private boolean isKoreanOrder(VuittonOrderImpl order) {
        return getSiteIdFinderService().isKoreaSite(order.getSiteId());
    }

    /**
     * To check for Chinese order.
     *
     * @param order the order
     * @return true, if is chinese order
     */
    private boolean isChineseOrder(VuittonOrderImpl order) {
        return getSiteIdFinderService().isChinaSite(order.getSiteId());
    }

    /**
     * Checks if is canadian FR order.
     *
     * @param order the order
     * @return true, if is canadian FR order
     */
	public boolean isCanadianFROrder(VuittonOrder order) {
		boolean isCanadianFROrder = false;
	      RepositoryItem orderContext = (RepositoryItem) order.getContext();
        if(orderContext == null){
            if (isLoggingError()){
                logError("In isCanadianOrder ::: ["+order.getId()+"] "+"No order context found");
            }
            return isCanadianFROrder;
        }
        String storeReference = orderContext.getPropertyValue(RepositoryConstants.CONTEXT_STORE_REFERENCE_PROPERTY_NAME).toString();
        String language = orderContext.getPropertyValue(RepositoryConstants.CONTEXT_LANGAGE_PROPERTY_NAME).toString();
        if (isLoggingDebug()) {
			logDebug("In isCanadianOrder ::: order : " + order.getId() + " storeReference : " + storeReference + " language : " + language);
		}
        if (STORE_CA.equalsIgnoreCase(storeReference) && FRA.equalsIgnoreCase(language)) {
			isCanadianFROrder = true;
		}
		return isCanadianFROrder;
	}

    /**
     * Checks if is canadian order.
     *
     * @param order the order
     * @return true, if is canadian order
     */
	public boolean isCanadianOrder(VuittonOrderImpl order) {
		  boolean isCanadianOrder = false;
	      RepositoryItem orderContext = (RepositoryItem) order.getContext();
          if(orderContext == null){
              if (isLoggingError()){
                  logError("In isCanadianOrder ::: ["+order.getId()+"] "+"No order context found");
              }
              return isCanadianOrder;
          }
          String storeReference = orderContext.getPropertyValue(RepositoryConstants.CONTEXT_STORE_REFERENCE_PROPERTY_NAME).toString();
          if (STORE_CA.equalsIgnoreCase(storeReference)) {
        	  isCanadianOrder = true;
          }
		return isCanadianOrder;
	}

    /**
     * Checks if is US order.
     *
     * @param order the order
     * @return true, if is US order
     */
    public boolean isUSOrder(VuittonOrderImpl order) {
        boolean isUSOrder = false;
        RepositoryItem orderContext = (RepositoryItem) order.getContext();
        if (orderContext == null) {
            if (isLoggingError()) {
                logError("In isCanadianOrder ::: [" + order.getId() + "] " + "No order context found");
            }
            return isUSOrder;
        }
        String storeReference = orderContext.getPropertyValue(RepositoryConstants.CONTEXT_STORE_REFERENCE_PROPERTY_NAME).toString();
        if (SHOP_US.equalsIgnoreCase(storeReference)) {
            isUSOrder = true;
        }
        return isUSOrder;
    }

    /**
     * Checks if is GB order.
     *
     * @param order the order
     * @return true, if is GB order
     */
    public boolean isGBOrder(VuittonOrderImpl order) {
        boolean isGBOrder = false;
        RepositoryItem orderContext = (RepositoryItem) order.getContext();
        if (orderContext == null) {
            if (isLoggingError()) {
                logError("In isGBOrder ::: [" + order.getId() + "] " + "No order context found");
            }
            return isGBOrder;
        }
        String storeReference = orderContext.getPropertyValue(RepositoryConstants.CONTEXT_STORE_REFERENCE_PROPERTY_NAME).toString();
        if (SHOP_GB.equalsIgnoreCase(storeReference)) {
            isGBOrder = true;
        }
        return isGBOrder;
    }

    public boolean isJPOrder(VuittonOrderImpl order) {
        boolean isJPOrder = false;
        RepositoryItem orderContext = (RepositoryItem) order.getContext();
        if (orderContext == null) {
            if (isLoggingError()) {
                logError("In isJPOrder ::: [" + order.getId() + "] " + "No order context found");
            }
            return isJPOrder;
        }
        String storeReference = orderContext.getPropertyValue(RepositoryConstants.CONTEXT_STORE_REFERENCE_PROPERTY_NAME).toString();
        if (SHOP_JP.equalsIgnoreCase(storeReference)) {
            isJPOrder = true;
        }
        return isJPOrder;
    }

    /**
     * Checks if credit details have to be displayed for the current site.
     */
    private boolean displayCreditCardDetails(VuittonOrderImpl order, String ccNumber) {
        if (StringUtils.isEmpty(ccNumber)) {
            return false;
        }

        RepositoryItem orderContext = order.getContext();
        String storeReference = String.valueOf(orderContext.getPropertyValue(RepositoryConstants.CONTEXT_STORE_REFERENCE_PROPERTY_NAME));
        return getSitesToDisplayCreditCard().contains(storeReference);
    }

    /**
     * Gets the time stamp.
     *
     * @param orderDate the order date
     * @return the time stamp
     */
	private String getTimeStamp(Date orderDate) {
		Format formatter = new SimpleDateFormat("MMMM");
		String month = formatter.format(orderDate);
		formatter = new SimpleDateFormat("dd");
		int day = orderDate.getDate();
		String suffix = getDayOfMonthSuffix(day);
		String date = String.valueOf(day) + suffix;
		formatter = new SimpleDateFormat("yyyy");
		String year = formatter.format(orderDate);
		formatter = new SimpleDateFormat("HH:mm");
		String time = formatter.format(orderDate);
		String timeStamp = month + " " + date + " " + year + "  " + time;
		return timeStamp;

	}

    /**
     * Gets the date.
     *
     * @param orderDate the order date
     * @return the date
     */
    private String getDate(Date orderDate) {
        Format formatter = new SimpleDateFormat("MMMM");
        String month = formatter.format(orderDate);
        formatter = new SimpleDateFormat("dd");
        int day = orderDate.getDate();
        String suffix = getDayOfMonthSuffix(day);
        String date = String.valueOf(day) + suffix;
        formatter = new SimpleDateFormat("yyyy");
        String year = formatter.format(orderDate);
        formatter = new SimpleDateFormat("HH:mm");
        String currentDate = month + " " + date + " " + year;
        return currentDate;

    }

    /**
     * Gets the locale specific date.
     *
     * @param orderDate the order date
     * @param order the order
     * @param countryCode the country code
     * @return the locale specific date
     */
    private String getLocaleSpecificDate(Date orderDate, VuittonOrderImpl order, String countryCode) {
        // Map<String, String> orderContext = retrieveOrderContext(order);
        String pattern = getDatePattern().get(countryCode);
        String localeForTranslation = getLanguageTranslations().get(countryCode);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, new Locale(localeForTranslation));
        String date = simpleDateFormat.format(orderDate);
        return date;

    }

    /**
     * Gets the day of month suffix.
     *
     * @param n the n
     * @return the day of month suffix
     */
	String getDayOfMonthSuffix(final int n) {
		if (n >= 11 && n <= 13) {
			return "th";
		}
		switch (n % 10) {
		case 1:
			return "st";
		case 2:
			return "nd";
		case 3:
			return "rd";
		default:
			return "th";
		}
	}

    /**
     * Write byte stream to file.
     *
     * @param os the os
     * @param fileName the file name
     * @param filePath the file path
     * @throws IOException Signals that an I/O exception has occurred.
     */
	public void writeByteStreamToFile(ByteArrayOutputStream os, String fileName, String filePath) throws IOException {
		FileOutputStream fos = null;
		try {
			fileName = "writeByteStreamToFile.pdf";
			logDebug("writeByteStreamToFile : " + filePath + fileName);
			File outputFile = new File(filePath + fileName);
			fos = new FileOutputStream(outputFile);
			os.writeTo(fos);
			logDebug("Exiting writeByteStreamToFile in PDFGenerationTools");
		} catch (Exception e) {
            logError("File Exception Error " + e);
			e.printStackTrace();
		} finally {
			fos.flush();
			fos.close();
		}
	}
	
    /**
     * Generate XML for HS.
     *
     * @param orderId the order id
     * @param sg the sg
     * @return the string buffer
     */
	public StringBuffer generateXMLForHS(String orderId, VuittonShippingGroupImpl sg){

		String ctx = "In downloadHSPDF method of DownloadHSPDF : ";
		StringBuffer hsXml = new StringBuffer();
		VuittonOrderImpl order = null;
		try {
			order = (VuittonOrderImpl) getOrderManager().loadOrder(orderId);
		} catch (CommerceException e) {
			logError(ctx + "CommerceException loading order id = " + orderId, e);
		}
		// build the xml String containing the hot stamping

		hsXml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?><hotstampingInfo>");
		
		List<VuittonCommerceItem> commerceItems = new ArrayList<VuittonCommerceItem>();
        commerceItems = sg.getHotStampedCommerceItems();

		if (CollectionUtils.isNotEmpty(commerceItems)) {
			for (VuittonCommerceItem commerceItem : commerceItems) {
				if (getPersonalizationManager().isHS(commerceItem)) {
					String initials = getPersonalizationManager().getIHSInitials(commerceItem, Boolean.FALSE);
					hsXml.append("<hotstamping>");
					if (sg != null) {
						hsXml.append("<order>");
						hsXml.append(order.getId() + " - " + sg.getId());
						hsXml.append("</order>");
					}
					hsXml.append("<letters>");
					hsXml.append(initials);
					hsXml.append("</letters>");
					String color = getPersonalizationManager().getIHSColorName(commerceItem);
					hsXml.append("<color>");
					hsXml.append(color);
					hsXml.append("</color>");
					Boolean isDotPresent = isDotPresent(commerceItem);
					String dotPresent = "NO";
					if (isDotPresent) {
						dotPresent = "YES";
					}
					hsXml.append("<dots>");
					hsXml.append(dotPresent);
					hsXml.append("</dots>");
					String displayAlignment = getPersonalizationManager().getIHSAlignment(commerceItem);
					hsXml.append("<display>");
					hsXml.append(displayAlignment.toUpperCase());
					hsXml.append("</display>");
					String skuId = commerceItem.getCatalogRefId();
					Sku sku = getSkuFinder().findSkuById(skuId);
					String imageUrl = populateImage(sku.getHotstampView());
					if (null == imageUrl) {
						hsXml.append("<image>");
						hsXml.append("NO IMAGE FOUND");
						hsXml.append("</image>");
					} else {
						hsXml.append("<image>");
						hsXml.append(imageUrl);
						hsXml.append("</image>");
					}
                    // Default image height\width
                    int imageWidth = 750;
                    int imageHeight = 750;
                    int imageWidthOnPDF = 4;
                    int imageHeightOnPDF = 4;

                    // Get actual image size
                    if (null != imageUrl) {
                        URL url;
                        try {
                            url = new URL(imageUrl);
                            URLConnection conn = url.openConnection();
							vlogDebug("Image url {0} and url connection {1}", imageUrl, conn);
                            InputStream in = conn.getInputStream();
                            BufferedImage image = ImageIO.read(in);
							if(image != null) {
                             // you can get size dimesion
                             imageWidth = image.getWidth();
                             imageHeight = image.getHeight();
							}
                        } catch (IOException e) {
                            vlogError("Exception while retriving image dimensions {0}", e);
                        }

                    }

					String initialCoordinates = sku.getInitialCoordinates();
					String[] CoordinatesArr = initialCoordinates.split(";");
                    String coordinatesX = CoordinatesArr[0];
                    // calculate value with respect to 400pt on PDF
                    double newCoordinatesX = (((Double.valueOf(coordinatesX) * 100) / imageWidth) * imageWidthOnPDF);
                    // round of value to 2 decimal points
                    newCoordinatesX = newCoordinatesX * 100;
                    newCoordinatesX = Math.round(newCoordinatesX);
                    newCoordinatesX = newCoordinatesX / 100;
                    int stringWidthInPoints = 15;
                    int initialsLength = initials.length();
                    if (initialsLength == 2) {
                        stringWidthInPoints = 10;
                    } else if (initialsLength == 1) {
                        stringWidthInPoints = 5;
                    }
                    // deduct string length to get correct starting point
                    newCoordinatesX = newCoordinatesX - stringWidthInPoints;
					String coordinatesY = CoordinatesArr[1];
                    // calculate value with respect to 400pt on PDF + Add 1cm(28.34pt) as top margin
                    double newCoordinatesY = (((Double.valueOf(coordinatesY) * 100) / imageHeight) * imageHeightOnPDF) + 28.34;
                    // round of value to 2 decimal points
                    newCoordinatesY = newCoordinatesY * 100;
                    newCoordinatesY = Math.round(newCoordinatesY);
                    newCoordinatesY = newCoordinatesY / 100;
					hsXml.append("<coordinatesX>");
					hsXml.append(newCoordinatesX);
					hsXml.append("</coordinatesX>");
					hsXml.append("<coordinatesY>");
					hsXml.append(newCoordinatesY);
					hsXml.append("</coordinatesY>");
					hsXml.append("</hotstamping>");
				}
			}
        } else {
            logInfo("CommerceItem not of type HS or engraving. Won't be printing HS PDF");
		}
		hsXml.append("</hotstampingInfo>");
		if (isLoggingDebug()) {
			logDebug("hot stamping xml : " + hsXml.toString());
		}
		return hsXml;
	}

    /**
     * Generate XML for engraving.
     *
     * @param orderId the order id
     * @param sg the sg
     * @return the string buffer
     */
    public StringBuffer generateXMLForEngraving(String orderId, VuittonShippingGroupImpl sg) {

        String ctx = "In downloadHSPDF method of DownloadHSPDF : ";
        StringBuffer engravingXml = new StringBuffer();
        VuittonOrderImpl order = null;
        boolean formatEngravedDate = false;
        try {
            order = (VuittonOrderImpl) getOrderManager().loadOrder(orderId);
        } catch (CommerceException e) {
            logError(ctx + "CommerceException loading order id = " + orderId, e);
        }
        // build the xml String containing the hot stamping

        engravingXml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?><engravingInfo>");

        List<VuittonCommerceItem> commerceItems = new ArrayList<VuittonCommerceItem>();
        commerceItems = sg.getEngravedCommerceItems();
        if (CollectionUtils.isNotEmpty(commerceItems)) {
            for (VuittonCommerceItem commerceItem : commerceItems) {
                if (getPersonalizationManager().isHS(commerceItem) || getPersonalizationManager().isEngravedItem(commerceItem)) {
                    String initials = getPersonalizationManager().getIHSInitials(commerceItem, Boolean.FALSE);
                    String datelib = getPersonalizationManager().getEngravingDatelib(commerceItem);
                    String letters = "";
                    String Intituleletter = "";
                    String engravedText = "";

                    Boolean isDotPresent = isDotPresent(commerceItem);
                    if (StringUtils.isNotEmpty(initials)) {
                        letters = initials;
                        Intituleletter = "LETTERS";
                        if (isDotPresent) {
                            StringBuilder initial = new StringBuilder(letters);
                            for (int i = 1; i < initial.length(); i = i + 2) {
                                initial.insert(i, '.');
                            }
                            engravedText = initial.toString();
                        }
                    }
                    if (StringUtils.isNotEmpty(datelib)) {
                        letters = letters + datelib;
                        Intituleletter = "DATE";
                    }
                    engravingXml.append("<engraving>");
                    if (sg != null) {
                        engravingXml.append("<order>");
                        engravingXml.append(order.getId() + " - " + sg.getId());
                        engravingXml.append("</order>");
                    }

                    engravingXml.append("<Intituleletter>");
                    engravingXml.append(Intituleletter);
                    engravingXml.append("</Intituleletter>");

                    engravingXml.append("<letters>");
                    engravingXml.append(letters);
                    engravingXml.append("</letters>");

                    engravingXml.append("<engravedtext>");
                    if (StringUtils.isNotBlank(engravedText)) {
                        engravingXml.append(engravedText);
                    } else {
                        engravingXml.append(letters);
                    }
                    engravingXml.append("</engravedtext>");

                    String dotPresent = "NO";
                    if (isDotPresent) {
                        dotPresent = "YES";
                    }
                    engravingXml.append("<dots>");
                    engravingXml.append(dotPresent);
                    engravingXml.append("</dots>");

                    Boolean isFinishGold = isFinishGold(commerceItem);
                    String finishGold = "NO";
                    if (isFinishGold != null && isFinishGold) {
                        finishGold = "YES";
                        engravingXml.append("<color>");
                        engravingXml.append("Gold");
                        engravingXml.append("</color>");
                    } else {
                        engravingXml.append("<color>");
                        engravingXml.append("Silver");
                        engravingXml.append("</color>");
                    }
                    engravingXml.append("<finishgold>");
                    engravingXml.append(finishGold);
                    engravingXml.append("</finishgold>");

                    
                    String skuId = commerceItem.getCatalogRefId();
                    Sku sku = getSkuFinder().findSkuById(skuId);
                    String imageUrl = populateImage(sku.getFrontView());
                    if (null == imageUrl) {
                        engravingXml.append("<image>");
                        engravingXml.append("NO IMAGE FOUND");
                        engravingXml.append("</image>");
                    } else {
                        engravingXml.append("<image>");
                        engravingXml.append(imageUrl);
                        engravingXml.append("</image>");
                    }
                    String initialCoordinates = sku.getInitialCoordinates();
                    if (initialCoordinates != null) {
                        if (StringUtils.isNotBlank(datelib) && isDotPresent(commerceItem)) {
                            formatEngravedDate = true;
                        }
                        String[] CoordinatesArr = initialCoordinates.split(";");
                        String coordinatesX = CoordinatesArr[0];
                        String newCoordinatesX = coordinatesConversion(coordinatesX);
                        String coordinatesY = CoordinatesArr[1];
                        String newCoordinatesY = coordinatesConversionEngraving(coordinatesY, formatEngravedDate);
                        engravingXml.append("<coordinatesX>");
                        engravingXml.append(newCoordinatesX);
                        engravingXml.append("</coordinatesX>");
                        engravingXml.append("<coordinatesY>");
                        engravingXml.append(newCoordinatesY);
                        engravingXml.append("</coordinatesY>");
                    } else {
                        vlogError("In engraving xml - No coordinates found : orderId {0}, skuId:{1} ", order.getId(), skuId);
                        engravingXml.append("<coordinatesX>");
                        engravingXml.append("");
                        engravingXml.append("</coordinatesX>");
                        engravingXml.append("<coordinatesY>");
                        engravingXml.append("");
                        engravingXml.append("</coordinatesY>");
                    }
                    engravingXml.append("</engraving>");
                }
            }
        }
        engravingXml.append("</engravingInfo>");
        if (isLoggingDebug()) {
            logDebug("engraving xml : " + engravingXml.toString());
        }
        return engravingXml;
    }

    /**
     * Coordinates conversion engraving.
     *
     * @param coordinates the coordinates
     * @return the string
     */
    private String coordinatesConversionEngraving(String coordinates, boolean formatEngravedDate) {
        String coordinatesStringVal = "0";
        int reductionFactor = 0;
        if (!StringUtils.isBlank(coordinates)) {
            double coordinatesDoubleVal = Double.parseDouble(coordinates);
            double newCoordinatesDoubleVal = (coordinatesDoubleVal / 10) / 2;
            if (formatEngravedDate) {
                reductionFactor = getCoordReductionFactor();
            }
            coordinatesStringVal = Double.toString(newCoordinatesDoubleVal - reductionFactor);
        }
        return coordinatesStringVal;
    }

    /**
     * Coordinates conversion.
     *
     * @param coordinates the coordinates
     * @return the string
     */
	private String coordinatesConversion(String coordinates) {
		String coordinatesStringVal = "0";
		if (!StringUtils.isBlank(coordinates)) {
		double coordinatesDoubleVal = Double.parseDouble(coordinates);
		double newCoordinatesDoubleVal = coordinatesDoubleVal/10;
		coordinatesStringVal = Double.toString(newCoordinatesDoubleVal);
		}
		return coordinatesStringVal;
	}

    /**
     * Checks if is dot present.
     *
     * @param commerceItem the commerce item
     * @return the boolean
     */
	private Boolean isDotPresent(VuittonCommerceItem commerceItem) {
        Boolean isDotPresent = Boolean.FALSE;
        if (getPersonalizationManager().isHS(commerceItem) || getPersonalizationManager().isEngravedItem(commerceItem)) {
			RepositoryItem personalizedRepItem = commerceItem.getHSOptions();
            isDotPresent = (Boolean) personalizedRepItem.getPropertyValue(RepositoryConstants.HS_DOT);
            if (null == isDotPresent) {
                isDotPresent = Boolean.FALSE;
            }
		}
        return isDotPresent;
	}

    /**
     * Checks if is finish gold.
     *
     * @param commerceItem the commerce item
     * @return the boolean
     */
    private Boolean isFinishGold(VuittonCommerceItem commerceItem) {
        if (getPersonalizationManager().isEngravedItem(commerceItem)) {
            RepositoryItem personalizedRepItem = commerceItem.getHSOptions();
            Boolean isFinishGold = (Boolean) personalizedRepItem.getPropertyValue(RepositoryConstants.ENGRAVING_FINISHGOLD);
            return isFinishGold;
        }
        return false;
    }

    /**
     * Generate XML for perfume sample.
     *
     * @param orderId the order id
     * @param sg the sg
     * @return the string buffer
     */
	public StringBuffer generateXMLForPerfumeSample(String orderId, VuittonShippingGroupImpl sg) {
		String ctx = "In downloadP55SamplePDF method of DownloadP55SamplePDF : ";
		StringBuffer p55Xml = new StringBuffer();
		VuittonOrderImpl order = null;
		try {
			order = (VuittonOrderImpl) getOrderManager().loadOrder(orderId);
		} catch (CommerceException e) {
			logError(ctx + "CommerceException loading order id = " + orderId, e);
		}

		if (order != null) {

			boolean containsPerfume = getPerfumeManager().sgContainsPerfume(sg);

			if (containsPerfume) {
				// build the xml String containing the P55 Sample Information
				p55Xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?><perfumeSamples>");
				p55Xml.append("<shippingGroupId>");
				p55Xml.append(sg.getId());
				p55Xml.append("</shippingGroupId>");
				String[] sampleArray = retrievePerfumeSamples(order);
                if (sampleArray != null) {
                    List<String> sampleList = Arrays.asList(sampleArray);
                    if (CollectionUtils.isNotEmpty(sampleList)) {
                        List<VuittonCommerceItem> commerceItems = new ArrayList<VuittonCommerceItem>();
                        commerceItems = order.getCommerceItems();
                        if (CollectionUtils.isNotEmpty(commerceItems)) {
                            for (String sample : sampleList) {
                                if (isLoggingDebug()) {
                                    logDebug("sample : " + sample);
                                }
                                p55Xml.append("<perfumeSample>");
                                String skuCode = getSKUCode(sample);
                                p55Xml.append("<code>");
                                p55Xml.append(skuCode);
                                p55Xml.append("</code>");
                                String sampleName = getSampleName(sample);
                                p55Xml.append("<name>");
                                p55Xml.append(sampleName);
                                p55Xml.append("</name>");
                                for (VuittonCommerceItem commerceItem : commerceItems) {
                                    if (isLoggingDebug()) {
                                        logDebug("sample id : " + sample + "skuCode : " + skuCode + " , commerceItem.getCatalogRefId() : "
                                                + commerceItem.getCatalogRefId());
                                    }
                                    if (skuCode.equalsIgnoreCase(commerceItem.getCatalogRefId())) {
                                        if (isLoggingDebug()) {
                                            logDebug("sample id : " + sample + " commerce item match found");
                                        }
                                        p55Xml.append("<quantity>");
                                        p55Xml.append(commerceItem.getQuantity());
                                        p55Xml.append("</quantity>");

                                    }
                                }

                                p55Xml.append("</perfumeSample>");
                            }
                        }
                    }
                    p55Xml.append("</perfumeSamples>");
                    if (isLoggingDebug()) {
                        logDebug("P55 sample xml : " + p55Xml.toString());
                    }
				}
			} else {
				if (isLoggingDebug()) {
					logDebug("No perfume items in order : " + orderId + " shippingGroupId : " + sg);
				}
			}
		}
		return p55Xml;
	}

    /**
     * Gets the SKU code.
     *
     * @param sample the sample
     * @return the SKU code
     */
	private String getSKUCode(String sample) {
		int startIndex = sample.indexOf("(");
		int endIndex = sample.indexOf(")");
		return sample.substring(startIndex + 1, endIndex);

	}

    /**
     * Gets the sample name.
     *
     * @param sample the sample
     * @return the sample name
     */
	private String getSampleName(String sample) {
		int startIndex = sample.indexOf("(");
        String sampleName = sample.substring(0, startIndex - 1);
        sampleName = replaceSpecialChar(sampleName);
        return sampleName;

	}

    /**
     * Retrieve perfume samples.
     *
     * @param order the order
     * @return the string[]
     */
	public String[] retrievePerfumeSamples(final VuittonOrder order) {
		final String samples = order.getPerfumeSamples();
		if (isLoggingDebug()) {
			logDebug("PerfumeSamples : " + samples);
		}
		final List<Product> sampleList = new ArrayList<Product>();
		String[] sampleArray = null;
		if (StringUtils.isNotBlank(samples)) {
			sampleArray = StringUtils.split(samples, ValidationConstants.LIST_SEPARATOR);
		}
		return sampleArray;
	}

    /**
     * Checks if is HS item present.
     *
     * @param orderId the order id
     * @param sg the sg
     * @return true, if is HS item present
     */
	public boolean isHSItemPresent(String orderId, VuittonShippingGroupImpl sg) {
		boolean isHSItemPresent = false;
		VuittonOrderImpl order = null;
		List<VuittonCommerceItem> commerceItems = new ArrayList<VuittonCommerceItem>();
		try {
			order = (VuittonOrderImpl) getOrderManager().loadOrder(orderId);
			commerceItems = order.getCommerceItems();
			for (VuittonCommerceItem commerceItem : commerceItems) {
				if (getPersonalizationManager().isHS(commerceItem)) {
					if (isLoggingDebug()) {
						logDebug("In isHSItemPresent : orderId " + orderId + " commerceItem : " + commerceItem.getId()
								+ " hot stamping is present");
					}
					isHSItemPresent = true;
				}
			}
		} catch (CommerceException e) {
			logError("CommerceException in isHSItemPresent : orderId : " + orderId + " " + e);
		}

		return isHSItemPresent;
	}

    /**
     * Generate file name.
     *
     * @param shippingGroupID the shipping group ID
     * @param fileType the file type
     * @return the string
     */
	public String generateFileName(String shippingGroupID, String fileType) {
		String currentDateAndTime = getCurrentDateAndTime();
		String fileName = shippingGroupID.concat("_").concat(fileType).concat("_").concat(currentDateAndTime).concat(".pdf");
		return fileName;
	}

    /**
     * Gets the current date and time.
     *
     * @return the current date and time
     */
	private String getCurrentDateAndTime() {
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMdd_HHmm");
		Date now = new Date();
		String strDate = sdfDate.format(now);
		return strDate;
	}

    /**
     * Generate file name for engraving.
     *
     * @param shippingGroupID the shipping group ID
     * @param fileType the file type
     * @return the string
     */
    public String generateFileNameForEngraving(String shippingGroupID, String fileType) {
        String currentDateAndTime = getCurrentDateAndTimeForEngraving();
        String fileName = shippingGroupID.concat("_").concat(fileType).concat("_").concat(currentDateAndTime).concat(".pdf");
        return fileName;
    }

    /**
     * Gets the current date and time for engraving.
     *
     * @return the current date and time for engraving
     */
    private String getCurrentDateAndTimeForEngraving() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMdd_HHmm");
        Date now = new Date();
        now = DateUtils.addMinutes(now, 5);
        String strDate = sdfDate.format(now);
        return strDate;
    }


    /**
     * Generate XML for gift message.
     *
     * @param pOrderId the order id
     * @param sg the sg
     * @return the string buffer
     * @throws IOException Signals that an I/O exception has occurred.
     */
	public StringBuffer generateXMLForGiftMessage(String pOrderId, VuittonShippingGroupImpl sg) throws IOException {

		String ctx = "downloadGiftCardPDF - ";

		if (StringUtils.isEmpty(pOrderId) || sg == null) {
			logError(ctx + "pOrderId and/or pShippingGroupId is null or empty");
		}

		VuittonOrderImpl order = null;
		try {
			order = (VuittonOrderImpl) getOrderManager().loadOrder(pOrderId);
		} catch (CommerceException ce) {
			logError(ctx + "CommerceException loading order id = " + pOrderId, ce);
		}
        boolean isChineseOrder = isChineseOrder(order);
        boolean isKoreanOrder = isKoreanOrder(order);
		// build the xml String containing the Gift message
		StringBuffer giftXml = new StringBuffer();
		giftXml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?><gift-message>");

		String country = null;

		country = (String) sg.getRepositoryItem().getPropertyValue(RepositoryConstants.COUNTRY_ITEM_NAME);

		String giftMsg = sg.getGiftCardMessage();
		if (giftMsg == null) {
			giftMsg = "no gift message";
			if (isLoggingDebug()) {
				logDebug("No gift message found for the order" + pOrderId);
			}
		}

		VuittonShippingGroupManager sgm = (VuittonShippingGroupManager) getOrderManager().getShippingGroupManager();
		
		//Calling the country specific method for splitting gift message in lines according to locale rules
        if (isChineseOrder) {
            vlogDebug("Chinese Order: Calling splitGiftMessageInLinesCN");
            List<String> lines = sgm.splitGiftMessageInLinesCN(giftMsg);
            for (String line : lines) {
                giftXml.append("<msg-line>" + CDATAS + line + CDATAE + "</msg-line>");
            }
        } else if (isKoreanOrder) {
            vlogDebug("Korean Order: Calling splitGiftMessageInLines_ForKR");
            List<String> lines = sgm.splitGiftMessageInLines_ForKR(giftMsg, country);
            for (String line : lines) {
                giftXml.append("<msg-line>" + CDATAS + line + CDATAE + "</msg-line>");
            }
        } else {
            vlogDebug("Calling splitGiftMessageInLines for country [{0}]", country);
            List<String> lines = sgm.splitGiftMessageInLines(giftMsg, country);
            for (String line : lines) {
                giftXml.append("<msg-line>" + CDATAS + line + CDATAE + "</msg-line>");
            }
        }

		giftXml.append("</gift-message>");
		if (isLoggingDebug()) {
			logDebug("gift message xml : " + giftXml.toString());
		}

		return giftXml;
	}
	
    /**
     * Populate image.
     *
     * @param media the media
     * @return the string
     */
	private String populateImage(Media media) {
        if (media == null) {
            return null;
        }
        String imageUrl = getScene7Manager().getImageSeoUrl(null, media.getUrl(), true);
        return imageUrl;
    }

    /**
     * Gets the order manager.
     *
     * @return the order manager
     */
    public OrderManager getOrderManager() {
        return orderManager;
    }

    /**
     * Sets the order manager.
     *
     * @param orderManager the new order manager
     */
    public void setOrderManager(OrderManager orderManager) {
        this.orderManager = orderManager;
    }

    /**
     * Gets the product catalog.
     *
     * @return the product catalog
     */
    public Repository getProductCatalog() {
        return productCatalog;
    }

    /**
     * Sets the product catalog.
     *
     * @param productCatalog the new product catalog
     */
    public void setProductCatalog(Repository productCatalog) {
        this.productCatalog = productCatalog;
    }

    /**
     * Gets the file path client receipt.
     *
     * @return the file path client receipt
     */
    public String getFilePathClientReceipt() {
        return filePathClientReceipt;
    }

    /**
     * Sets the file path client receipt.
     *
     * @param filePathClientReceipt the new file path client receipt
     */
    public void setFilePathClientReceipt(String filePathClientReceipt) {
        this.filePathClientReceipt = filePathClientReceipt;
    }

    /**
     * Gets the user repository.
     *
     * @return the user repository
     */
    public Repository getUserRepository() {
        return userRepository;
    }

    /**
     * Sets the user repository.
     *
     * @param userRepository the new user repository
     */
    public void setUserRepository(Repository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Gets the personalization manager.
     *
     * @return the personalization manager
     */
    public PersonalizationManager getPersonalizationManager() {
        return personalizationManager;
    }

    /**
     * Sets the personalization manager.
     *
     * @param personalizationManager the new personalization manager
     */
    public void setPersonalizationManager(PersonalizationManager personalizationManager) {
        this.personalizationManager = personalizationManager;
    }

    /**
     * Gets the perfume manager.
     *
     * @return the perfume manager
     */
    public PerfumeManager getPerfumeManager() {
        return perfumeManager;
    }

    /**
     * Sets the perfume manager.
     *
     * @param perfumeManager the new perfume manager
     */
    public void setPerfumeManager(PerfumeManager perfumeManager) {
        this.perfumeManager = perfumeManager;
    }

    /**
     * Gets the product manager.
     *
     * @return the product manager
     */
    public ProductManager getProductManager() {
        return productManager;
    }

    /**
     * Sets the product manager.
     *
     * @param productManager the new product manager
     */
    public void setProductManager(ProductManager productManager) {
        this.productManager = productManager;
    }

    /**
     * Gets the sku finder.
     *
     * @return the sku finder
     */
    public SkuFinder getSkuFinder() {
        return skuFinder;
    }

    /**
     * Sets the sku finder.
     *
     * @param skuFinder the new sku finder
     */
    public void setSkuFinder(SkuFinder skuFinder) {
        this.skuFinder = skuFinder;
    }

    /**
     * Gets the scene 7 manager.
     *
     * @return the scene 7 manager
     */
    public Scene7Manager getScene7Manager() {
        return scene7Manager;
    }

    /**
     * Sets the scene 7 manager.
     *
     * @param scene7Manager the new scene 7 manager
     */
    public void setScene7Manager(Scene7Manager scene7Manager) {
        this.scene7Manager = scene7Manager;
    }

    /**
     * Gets the country CN.
     *
     * @return the country CN
     */
    public String getCountryCN() {
        return countryCN;
    }

    /**
     * Sets the country CN.
     *
     * @param countryCN the new country CN
     */
    public void setCountryCN(String countryCN) {
        this.countryCN = countryCN;
    }

    /**
     * Gets the time zone helper.
     *
     * @return the time zone helper
     */
    public SiteTimeZoneHelper getTimeZoneHelper() {
        return timeZoneHelper;
    }

    /**
     * Sets the time zone helper.
     *
     * @param timeZoneHelper the new time zone helper
     */
    public void setTimeZoneHelper(SiteTimeZoneHelper timeZoneHelper) {
        this.timeZoneHelper = timeZoneHelper;
    }

    /**
     * Gets the efapiao helper.
     *
     * @return the efapiao helper
     */
    public EfapiaoHelper getEfapiaoHelper() {
        return efapiaoHelper;
    }

    /**
     * Sets the efapiao helper.
     *
     * @param efapiaoHelper the new efapiao helper
     */
    public void setEfapiaoHelper(EfapiaoHelper efapiaoHelper) {
        this.efapiaoHelper = efapiaoHelper;
    }

    /**
     * Gets the efapiao configuration.
     *
     * @return the efapiao configuration
     */
    public EfapiaoConfiguration getEfapiaoConfiguration() {
        return efapiaoConfiguration;
    }

    /**
     * Sets the efapiao configuration.
     *
     * @param efapiaoConfiguration the new efapiao configuration
     */
    public void setEfapiaoConfiguration(EfapiaoConfiguration efapiaoConfiguration) {
        this.efapiaoConfiguration = efapiaoConfiguration;
    }

    /**
     * Gets the site id finder service.
     *
     * @return the site id finder service
     */
    public VuittonSiteIdFinderService getSiteIdFinderService() {
        return siteIdFinderService;
    }

    /**
     * Sets the site id finder service.
     *
     * @param siteIdFinderService the new site id finder service
     */
    public void setSiteIdFinderService(VuittonSiteIdFinderService siteIdFinderService) {
        this.siteIdFinderService = siteIdFinderService;
    }

    /**
     * Gets the policy text.
     *
     * @return the policy text
     */
    public Map<String, String> getPolicyText() {
        return policyText;
    }

    /**
     * Sets the policy text.
     *
     * @param policyText the policy text
     */
    public void setPolicyText(Map<String, String> policyText) {
        this.policyText = policyText;
    }

    /**
     * Gets the date pattern.
     *
     * @return the date pattern
     */
    public Map<String, String> getDatePattern() {
        return datePattern;
    }

    /**
     * Sets the date pattern.
     *
     * @param datePattern the date pattern
     */
    public void setDatePattern(Map<String, String> datePattern) {
        this.datePattern = datePattern;
    }

    /**
     * Gets the language translations.
     *
     * @return the language translations
     */
    public Map<String, String> getLanguageTranslations() {
        return languageTranslations;
    }

    /**
     * Sets the language translations.
     *
     * @param languageTranslations the language translations
     */
    public void setLanguageTranslations(Map<String, String> languageTranslations) {
        this.languageTranslations = languageTranslations;
    }

    /**
     * Gets the configuration.
     *
     * @return the configuration
     */
    public Configuration getConfiguration() {
        return configuration;
    }

    /**
     * Sets the configuration.
     *
     * @param configuration the new configuration
     */
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    /**
     * Gets the lv logo.
     *
     * @return the lv logo
     */
    public String getLvLogo() {
        return lvLogo;
    }

    /**
     * Sets the lv logo.
     *
     * @param lvLogo the new lv logo
     */
    public void setLvLogo(String lvLogo) {
        this.lvLogo = lvLogo;
    }

    /**
     * Getter for siteFinder.
     * 
     * @return the siteFinder
     */
    public SiteFinder getSiteFinder() {
        return siteFinder;
    }

    /**
     * Setter for siteFinder.
     * 
     * @param siteFinder the siteFinder to set
     */
    public void setSiteFinder(SiteFinder siteFinder) {
        this.siteFinder = siteFinder;
    }

    /**
     * Gets the payment text en.
     *
     * @return the payment text en
     */
    public String getPaymentTextEn() {
        return paymentTextEn;
    }

    /**
     * Sets the payment text en.
     *
     * @param paymentTextEn the new payment text en
     */
    public void setPaymentTextEn(String paymentTextEn) {
        this.paymentTextEn = paymentTextEn;
    }

    /**
     * Gets the payment text fr.
     *
     * @return the payment text fr
     */
    public String getPaymentTextFr() {
        return paymentTextFr;
    }

    /**
     * Sets the payment text fr.
     *
     * @param paymentTextFr the new payment text fr
     */
    public void setPaymentTextFr(String paymentTextFr) {
        this.paymentTextFr = paymentTextFr;
    }

    public List<String> getSitesToDisplayCreditCard() {
        return mSitesToDisplayCreditCard;
    }

    public void setSitesToDisplayCreditCard(List<String> pSitesToDisplayCreditCard) {
        mSitesToDisplayCreditCard = pSitesToDisplayCreditCard;
    }

    public Repository getInternalProfileRepository() {
        return internalProfileRepository;
    }

    public void setInternalProfileRepository(Repository internalProfileRepository) {
        this.internalProfileRepository = internalProfileRepository;
    }
}
