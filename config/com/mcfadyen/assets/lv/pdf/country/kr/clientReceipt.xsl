<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:java="java" exclude-result-prefixes="fo">    
<xsl:output method="xml" version="1.0" omit-xml-declaration="no" indent="yes"/>

<!-- ========================= -->
<!-- This xsl generates client receipt from a client receipt XML file-->
<!-- ========================= -->
																<!-- Newer changes -->

 <!-- set the number of columns you want globally -->
    <xsl:param name="set-cols" select="'4'"/>
 <!-- variables for font  -->
 	<xsl:variable name="CourierFont">Courier</xsl:variable>
 	<xsl:variable name="ArialFont">Arial</xsl:variable>
 	<xsl:variable name="Batang">BatangChe</xsl:variable>   
 <!-- variables for font size -->
    <xsl:variable name="CourierFontSize10">10pt</xsl:variable>
    <xsl:variable name="CourierFontSize9">9pt</xsl:variable>
    <xsl:variable name="CourierFontSize8">8pt</xsl:variable>
    <xsl:variable name="CourierFontSize7.4">7.4pt</xsl:variable>
    <xsl:variable name="ArialFontSize7">7pt</xsl:variable>
    <xsl:variable name="CourierFontSize6.8">6.8pt</xsl:variable>
    <xsl:variable name="ArialFontSize6.5">6.5pt</xsl:variable>
    <xsl:variable name="CourierFontSize6.5">6.5pt</xsl:variable>
    <xsl:variable name="ClientCopy">CLIENT COPY</xsl:variable>
    <xsl:variable name="ClientReceipt">Client Receipt</xsl:variable>
    <xsl:variable name="ASSOCIATE">ASSOCIATE : </xsl:variable>
    <xsl:variable name="ReturnPolicy">교환 및 환불규정 : </xsl:variable>
    <xsl:variable name="fontSize">10pt</xsl:variable>
	<xsl:variable name="rowHeight">10pt</xsl:variable>
	<xsl:variable name="ShippingGroupID" select="/items/sender/OrderID"></xsl:variable>
	<xsl:variable name="TimeStamp" select="/items/sender/TimeStamp" ></xsl:variable>
	<xsl:variable name="Address1" select="/items/sender/Address1"></xsl:variable>
	<xsl:variable name="Address2" select="/items/sender/Address2"></xsl:variable>
	<xsl:variable name="Address3" select="/items/sender/Address3"></xsl:variable>
	<xsl:variable name="Name" select="/items/sender/Name"></xsl:variable>
	<xsl:variable name="OrderOrigin" select="/items/sender/OrderOrigin"></xsl:variable>
	<xsl:variable name="tps" >tps</xsl:variable>
	<xsl:variable name="Policy" select="/items/sender/Policy"></xsl:variable>

	<!-- Provides line in dots -->

 	<fo:leader leader-pattern="dots" />

	<xsl:template match="items">
	<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
	
    <fo:layout-master-set>
    <!--  Define the page margins -->
      <fo:simple-page-master master-name="giftCard" page-height="215.9mm" page-width="139.7mm" >
        <fo:region-body/>
      </fo:simple-page-master>
    </fo:layout-master-set>

    <!--  For the page layout refer to the master layout -->
    <fo:page-sequence master-reference="giftCard">
	
	
      <fo:flow flow-name="xsl-region-body">

	<!-- contents with relative positioning as given in excel -->
	<!-- reference - https://accounts.ecrion.com/help/products/xfrenderingserver/xfultrascalehelp3/absolut_positioning.htm -->
	
	
	<!-- Client Copy Text -->
	
		<fo:block-container position="absolute" top="4.568mm" left="5.575mm">
			<fo:block flow-name="" font-family="{$CourierFont}" font-size="10pt" font-weight="bold"><xsl:value-of select="$ClientCopy"/></fo:block>
		</fo:block-container>
	
	
	<!-- Client Receipt Text -->
	
		<fo:block-container position="absolute" top="20.118mm" left="6.075mm">
			<fo:block flow-name="" font-family="{$CourierFont}" font-size="{$CourierFontSize10}" font-weight="bold"><xsl:value-of select="$ClientReceipt"/></fo:block>
		</fo:block-container>
	

	<!-- OrderID/SG_ID -->
	<!-- To be populated -->

		<fo:block-container position="absolute" top="24.219mm" left="6.075mm">
			<fo:block flow-name="" font-family="{$CourierFont}" font-size="{$CourierFontSize7.4}"><xsl:value-of select="$ShippingGroupID"/></fo:block>
		</fo:block-container>
	
	
	<!-- Timestamp -->
	<!-- To be populated -->
	
		<fo:block-container position="absolute" top="20.118mm" left="85.35mm">
			<fo:block flow-name="" font-family="{$CourierFont}" font-size="{$CourierFontSize9}">
		 							<xsl:value-of select="$TimeStamp"/>
			</fo:block>
		</fo:block-container>

		<!-- Address-->
	<!-- To be populated -->
	
		<fo:block-container position="absolute" top="140mm" left="7mm">
			<fo:block flow-name="" font-family="{$Batang}" font-size="{$CourierFontSize6.5}">
						<xsl:value-of select="$Name"/>
			</fo:block>
			<fo:block flow-name="" font-family="{$Batang}" font-size="{$CourierFontSize6.5}">
			<xsl:value-of select="$Address3"/>
			</fo:block>
			<fo:block flow-name="" font-family="{$Batang}" font-size="{$CourierFontSize6.5}">
						<xsl:value-of select="$Address1"/>
			</fo:block>			
		</fo:block-container>
	<!-- ASSCOCIATE -->
	<!-- To be populated -->

		<fo:block-container position="absolute" top="132.118mm" left="7mm">
			<fo:block flow-name="" font-family="{$CourierFont}" font-size="{$ArialFontSize7}"><xsl:value-of select="concat($ASSOCIATE,$OrderOrigin)"/></fo:block>
			</fo:block-container>
		
		<!-- ReturnPolicy -->
	
		<fo:block-container position="absolute" top="135.05mm" left="7mm">
			<fo:block flow-name="" font-family="{$CourierFont}" font-size="{$ArialFontSize7}"><xsl:value-of select="$tps"/></fo:block>
		</fo:block-container>
	
		<!-- ReturnPolicy -->
	
		<!-- <fo:block-container position="absolute" top="152.118mm" left="5.875mm">
			<fo:block flow-name="" font-family="{$Batang}" font-size="{$CourierFontSize6.8}"><xsl:value-of select="$ReturnPolicy"/></fo:block>
		</fo:block-container> -->
		
		<!-- Policy -->

		
		<!-- <fo:block-container position="absolute" overflow="hidden" top="172.218mm" left="15.875mm">

            <fo:block>

               <fo:inline>

                 <xsl:value-of select="$Policy"/>

               </fo:inline>

             </fo:block>

         </fo:block-container>  -->
		
		
		<fo:block-container position="absolute" top="165.218mm" left="5.875mm" right="6mm" overflow="hidden">
			<fo:block linefeed-treatment="preserve" font-family="{$Batang}" font-size="{$CourierFontSize6.5}" text-align="center">
			
				<fo:inline>			
					<xsl:value-of select="$Policy"/>		
				</fo:inline>
			
			</fo:block>
			
		</fo:block-container>
	
	<!-- Policy -->
	<!-- To fill contents -->

		<fo:block-container position="absolute" top="177.218mm" left="5.875mm">
			<fo:block flow-name="" font-family="{$CourierFont}" font-size="{$CourierFontSize6.5}">
			</fo:block>
		</fo:block-container>
        <!-- This line defines the FONT USED for the GIFT MESSAGE: -->
		<fo:block-container position="absolute" top="30.818mm" left="6.075mm">
        <fo:block font-family="Courier" font-size="8pt" text-align="left" font-weight="bold">
		<fo:table>
		<fo:table-column column-width="9.25mm"/>
		<fo:table-column column-width="23.488mm"/>
		<fo:table-column column-width="74.6mm"/>
		<fo:table-column />
		<fo:table-body>
			<fo:table-row>      
      <fo:table-cell>
        <fo:block>
          QTY
        </fo:block>
      </fo:table-cell>
	  <fo:table-cell>
        <fo:block>
          CODE
        </fo:block>
      </fo:table-cell>
	  <fo:table-cell>
        <fo:block>
          DESCRIPTION
        </fo:block>
      </fo:table-cell>
	  <fo:table-cell>
        <fo:block>
          ASSOCIATE
        </fo:block>
      </fo:table-cell>
	  </fo:table-row>
		</fo:table-body>
		</fo:table>
		</fo:block>
		</fo:block-container> 
		<fo:block-container  position="absolute" top="31.185mm" left="4.825mm">
		<fo:block>
				<fo:leader leader-pattern="rule" leader-length="130mm" rule-style="solid" rule-thickness="1pt"/>		
				</fo:block>
		</fo:block-container>
				<fo:block-container position="absolute" top="35.818mm" left="6.075mm">
<fo:block font-family="Courier" font-size="8pt" text-align="left">
											
		<!-- count the needed rows -->
																						
		<fo:table>
		<fo:table-column column-width="9.25mm"/>
		<fo:table-column column-width="23.488mm"/>
		<fo:table-column column-width="74.6mm"/>
		<fo:table-column />
		
		
			<fo:table-body>
              <xsl:apply-templates select="item"/>
            </fo:table-body>
		</fo:table>			
		</fo:block>
		</fo:block-container>
		<xsl:variable name="rar" select="38 + count(//item)*5" >
		</xsl:variable>
		<fo:block-container  position="absolute" top="{$rar}mm" left="4.825mm">
		<fo:block>		
				<fo:leader leader-pattern="rule"  leader-length="130mm" rule-style="solid" rule-thickness="1pt"/>		
				</fo:block>
		</fo:block-container>
</fo:flow>
		</fo:page-sequence>

		</fo:root>
	</xsl:template>


<xsl:template match="item">

    <fo:table-row height="2.718mm">      
      <fo:table-cell>
        <fo:block>
          <xsl:value-of select="quantity"/>
        </fo:block>
      </fo:table-cell>
     
      <fo:table-cell>
        <fo:block>
          <xsl:value-of select="code"/>
        </fo:block>
      </fo:table-cell>   
      <fo:table-cell>
        <fo:block font-family="{$Batang}">
      <xsl:value-of select="description"/>
        </fo:block>
      </fo:table-cell>
	  <fo:table-cell>
        <fo:block>
      <xsl:value-of select="associate"/>
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
			
  </xsl:template>


<fo:block font-family="Futura Light" font-size="{$fontSize}" text-align="" font-weight="" >

</fo:block>





<!-- ========================= -->
<!-- Display a line of text    -->
<!-- ========================= -->
<xsl:template match="msg-line" >
  <fo:table-row  line-height="{$rowHeight}pt">
    <fo:table-cell height="{$rowHeight}pt"  display-align="center" ><!-- border-style="solid"-->
      <fo:block> 
        <xsl:value-of select="."/>
      </fo:block>
    </fo:table-cell> 
  </fo:table-row>
</xsl:template> 

</xsl:stylesheet>
